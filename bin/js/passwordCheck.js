var pass_strength;
function IsEnoughLength(str,length)
{if ((str == null) || isNaN(length))return false;else if (str.length < length)return false;return true;
}
function HasMixedCase(passwd)
{
if(passwd.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))return true;else return false;
}
function HasNumeral(passwd)
{if(passwd.match(/[0-9]/))
return true;else return false;
}
function HasSpecialChars(passwd)
{
if(passwd.match(/.[!,@,#,$,%,^,&,*,?,_,~]/))return true;else return false;
}
function CheckPasswordStrength(pwd)
{
	// pass_strength = "<b><font style='color:olive'>" + pwd + "</font></b>"
	
	if (IsEnoughLength(pwd,14) && HasMixedCase(pwd) && HasNumeral(pwd) && HasSpecialChars(pwd))pass_strength = "<b><font style='color:olive'>Very strong</font></b>";
	else if (IsEnoughLength(pwd,8) && HasMixedCase(pwd) && (HasNumeral(pwd) || HasSpecialChars(pwd)))pass_strength = "<b><font style='color:Blue'>Strong</font></b>";
else if (IsEnoughLength(pwd,8) && HasNumeral(pwd))
	{
		pass_strength = "<b><font style='color:Green'>Good</font></b>";
	}
else pass_strength = "<b><font style='color:red'>Weak</font></b>";

document.getElementById('pwd_strength').innerHTML = pass_strength;
}

//funciton to check it the user has entered the pass word correctly twice
function CheckPasswordMatch()
{
	var words;
	if (document.getElementById('password').value == document.getElementById("passTwo").value)
	{
		words = "passwords match."
		canGo = true;
	}
	else {
		words = "passwords do NOT match!"
		canGo = false;
	}
	$("#passTwoMess").html("<label class=\"message\">" + words + "</label>");
}