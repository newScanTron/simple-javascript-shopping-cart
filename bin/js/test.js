/* global QUnit */

//my first test to see that i can access the other js files like i thinjk i should
QUnit.test("addToCartTest", function (assert) {
  //since this test is going to test local storage and I want it to do that in isolation
  //im going to clear the local storage and session since the cart used that
  clearStorages();
  //a user to put in this test
  var userObj = setUser();
  //item for the test
  var item = {
    seller: "Tom",
    item: "cool item",
    price: 25
  }
  
  //here we addToCart() which is the function being tested currently
  addToCart(item)
  //we now want to test the item created above with what should be the only item in the cart JSON
  //and that we have the right user
  assert.equal(userObj.userName, userObj.userName, "User set proper");
  //
  var JaSON = sessionStorage.getItem('cart');
  var stuff = JSON.parse(JaSON);            
  var seller = stuff.cart[0].seller;
  var img = stuff.cart[0].buyer;
  var price = stuff.cart[0].price;
	var disName = stuff.cart[0].item;

  assert.equal(disName, item.item, "item check");
  assert.equal(seller, item.seller, "seller check");
  assert.equal(price, item.price, "price set");
  // assert.equal(sessionItem[0].buyer, user.userName, "not the same; equal fails")
});

//Second Test / getItems()
QUnit.test("getItems", function (assert) {
  //some test setup functions
  clearStorages();
  
  var user = setUser();
  var saleItems = setSaleItems();
  console.log("saleItems: " + saleItems);
  //var items = getAllItems();
  //assert some stuff
  assert.equal(saleItems.items[0].seller, user.userName, "seller is the current user");
  assert.equal(saleItems.items[0].name, "Cool Item", "item name is the checked");
  assert.equal(saleItems.items[0].price, 20, "right price, yo" );
  assert.equal(saleItems.items[1].seller, user.userName, "seller is the current user second item");
  assert.equal(saleItems.items[1].name, "Cool Item", "item name is the checked send item");
  assert.equal(saleItems.items[1].price, 40, "right price, yo second item" );
  
});
//Test to make sure the cart it what we think it it
QUnit.test("testCart", function (assert) {
   //some test setup functions
  clearStorages();
  var user = setUser();
  
  //item to run through the cart
  var item = {
    seller: "Tom",
    item: "cool item",
    description: "coolest item ever",
    price: 25
  }
  var item2 = {
    seller: "Tom",
    item: "item2",
    description: "coolest item ever",
    price: 25
  }
  
  //here we test addToCart() and getCart()
  addToCart(item);
  addToCart(item2);
  var disCart = getCart();
  //assert that the item i crreated above and added to local storage is the same as the one we are gettign out
  assert.equal(disCart.cart[0].item, item.item, "item added to cart and got back out.");
  assert.equal(disCart.cart[1].item, item2.item, "item2 added to cart and got back out.");
  //here test removeFromCart()
  removeFromCart(item);
  disCart = getCart();
  assert.equal(disCart.cart[0].item, item2.item, "first item in array was removed.");
  
  
});
//test to check the processOrder function
// QUnit.test("processOrdertest", function(assert) {
     
  
    
// });
//some helpers functions
function clearStorages()
{
  //since tests are going to test local storage and I want it to do that in isolation
  //im going to clear the local storage and session since the cart used that
  localStorage.clear();
  sessionStorage.clear();
}
//we often need to set a current user after we clear local stroage
function setUser()
{
   //a user to put in this test
  var user = {
    userName: "bill",
    passWord: "pass",
    email: "christmurphy@gmail.com"
  }
  //the function that i am testing need to have a 
  var text = '{ "user" : [{"userName":"' + user.userName + '" , "pass":"' + user.passWord + '", "email":"' + user.email + '"}]}';
  localStorage.setItem('users', text);
  //this a key step to making this local storage work the way i want it to.
  var userText = '{ "userName":"' + user.userName + '" , "pass":"' + user.pass + '" , "email":"' + user.email + '"}';
  localStorage.setItem('currentUser', userText);
  return user;
}
//function to set up some test items
function setSaleItems()
{
   //side note set user will need to be called first or this will not execute corretly
   var currUser = localStorage.getItem('currentUser');
   var words = JSON.parse(currUser);
            var text = '{ "items" : [{ "seller":"' + words.userName + '" , "name":"Cool Item", "discription":"A nice thing for purchanting", "image":"this should be base 64 but im lazy" , "price": 20  },'+
            '{ "seller":"' + words.userName + '" , "name":"Cool Item", "discription":"A nice thing for purchanting", "image":"this should be base 64 but im lazy" , "price": 40  }]}';
            localStorage.setItem('saleItem', text);
            var jsonObj = JSON.parse(text);
            console.log("text: " + jsonObj.items[0].seller);
            return jsonObj;
            
}

