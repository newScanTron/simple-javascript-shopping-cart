/* global jStorage */
/* global $ */
$(function () {

    $("input").focus(function () {
        $(this).css("background-color", "#aabbcc");
    });
    $("input").blur(function () {
        $(this).css("background-color", "#ffffff");
    });
    
    
//this should be seperated but it gets and displays the uploaded image.
    $("#fileInput").on('change', function () {

        if (typeof (FileReader) != "undefined") {

            var image_holder = $("#imagePreview");
            
            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image",
                    "id": "currImg"                
                    }).appendTo(image_holder);
             
                 localStorage.setItem('currImg', e.target.result);
  
  //debug
           // console.log(e.target.result);
            
            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
            
        } else {
            alert("This browser does not support FileReader.");
        }
        readURL(this); 
   
    });
});
//since this funciton in the read needs this i thought i would put it near
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imagePreview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function listItem() {
    var img = localStorage.getItem('currImg');
    var currUser = localStorage.getItem('currentUser');
    var words = JSON.parse(currUser);
  // alert("lister: " + words.userName);
    
    if (localStorage.getItem('saleItem') === null) {
            var text = '{ "items" : [{ "seller":"' + words.userName + '" , "name":"' + $("#itemName").val() + '", "discription":"' + $("#itemDescript").val() + '", "image":"' + img + '", "price": 20  }]}';
            localStorage.setItem('saleItem', text);
            console.log(localStorage.getItem('saleItem'));
    }
    else
    {
            var currItem = localStorage.getItem('saleItem');
            console.log("currItem" + currItem);
            var JaSON = JSON.parse(currItem);
            console.log("seller" + JSON.stringify(JaSON.items));
            var userText = '{ "seller":"' + words.userName + '" , "name":"' + $("#itemName").val() + '", "discription":"' + $("#itemDescript").val() + '", "image":"' + img + '"  }';
            var jason = JSON.parse(userText);
            JaSON['items'].push(jason);
            currItem = JSON.stringify(JaSON);
            localStorage.setItem('saleItem', currItem);
            //debugging .log
            var currArray = localStorage.getItem('saleItem');
            currArray = JSON.parse(currArray);
            console.log("sale items " + currArray.items.length);

    }
    //re set the page so we can list multiple items.
    $('#itemListingForm')[0].reset();
    $('#imagePreview').attr('src', '');
    $('#currImg').remove();
}
function showSignUp() {
    $("#signUpForm").toggle("fast");
    if ($('#loginForm').is(':visible')) {
        //Element is visible
        $("#loginForm").toggle("fast");

    }

}
function showLogin() {
    $("#loginForm").toggle("fast");
    if ($('#signUpForm').is(':visible')) {
        //Element is visible
        $("#signUpForm").toggle("fast");
    }
}


function loginCheck() {
    var pass = $("#passwordLogin").val();
    var userName = $("#userNameLogin").val();
    //need to check if in local storage there exists a
    //userName with that name.
    
   // console.log($("#userNameLogin").val());

    var userN = localStorage.getItem('users');
    var JaSON = JSON.parse(userN);

    //console.log(JaSON.user[0].userName);


    if (localStorage.getItem('users') === null) {
        alert("No users registered.");
    }
    else {
        //For loop to check all the users in the JSON obj
        for (var i = 0; i < JaSON.user.length; i++) {
            if (JaSON.user[i].userName == userName && JaSON.user[i].pass == pass) {
                alert("this user is ready");
                localStorage.setItem('currentUser', JSON.stringify(JaSON.user[i]));
                //alert("user?" + localStorage.getItem('currentUser'));
                showStore();

            }
        }
        $('#pwdLoginMessage').html("User Name or Password Are Wrong, or you are not registered; due to the lack of care taken we did not check to see which"
            + "of the two was incorrect they just both have to be right or it won't work.");
    }
}
//figured out how to split this up makes me feel kinda dumb now
function validateFields() 
{
    var canGo = true;
    var fields = getFields();
    //last name txt box
    if (fields.pass == "") {

        document.getElementById("lnMessage").innerHTML = "* Required";
        canGo = false;
    }
    else {
        document.getElementById("lnMessage").innerHTML = "";
    }
    //fist name txt box
    if (fields.userName == "") {
        document.getElementById("fnMessage").innerHTML = "* Required";
        canGo = false;
    }
    else {
        document.getElementById("fnMessage").innerHTML = "";
    }
    //email name txt box
    if (fields.email == "") {
        document.getElementById("emailMessage").innerHTML = "* Required";
        canGo = false;
    }
    else {
        document.getElementById("emailMessage").innerHTML = "";
    }
    //remail name txt box
    if (fields.remail == "") {
        document.getElementById("reemailMessage").innerHTML = "* Required";
        canGo = false;
    }
    if (fields.remail != fields.email) {
        document.getElementById("reemailMessage").innerHTML = "emails do not match";
        canGo = false;
    }
    else {
        document.getElementById("reemailMessage").innerHTML = "";
    }
    return canGo
}
//this function returns all the user information from the signup field
function getFields()
{
  return {
        pass : document.getElementById("password").value,
        userName : document.getElementById("userName").value,
        email : document.getElementById("emailTxt").value,
        remail : document.getElementById("renterEmail").value,
    }
}
//this thing is gross and really should be split up.
function signUp() {
    
    //check if all things are filled and if so set the cookie
    if (validateFields()) {
         var signUpUser = getFields();  
        if (localStorage.getItem('users') === null) {
            alert("User Registered.");
            //make a string version of a JSON array obj and setting it to 'users' in localStorage
            var text = '{ "user" : [{"userName":"' + signUpUser.userName + '" , "pass":"' +signUpUser.pass + '", "email":"' + signUpUser.email + '"}]}';
            localStorage.setItem('users', text);
            //parse the text to create a JSON array obj so i can pull the first and at this point only 
            //user in the array to  then stringafy and set as the current user in local storage.
            //could have just made a second string obviously but this was more fun.
            var JaSON = JSON.parse(text);
            var currentUser = JSON.stringify(JaSON.user[0]);
            localStorage.setItem('currentUser', currentUser);
        }
        else {
            var userN = localStorage.getItem('users');
            var JaSON = JSON.parse(userN);
            var userText = '{ "userName":"' + signUpUser.userName + '" , "pass":"' + signUpUser.pass + '" , "email":"' + signUpUser.email + '"}';
            localStorage.setItem('currentUser', userText);
           //this a key step to making this local storage work the way i want it to.
            userText = JSON.parse('{ "userName":"' + signUpUser.userName + '" , "pass":"' + signUpUser.pass + '" , "email":"' + signUpUser.email + '"}');
            JaSON['user'].push(userText);
            userN = JSON.stringify(JaSON);
            localStorage.setItem('users', userN);
            //debugging .log
            console.log(JSON.stringify(JaSON.user));
            alert("User Registered.");
        }
        $("#signUpForm").hide("fast");

        showStore();
    }
}
//function to show store and only have to rename it once 
//if i want to change the name of the html
function showStore() {
    window.location.replace("seller.html");
}

//function to take the user to the home page
function home() {
    window.location.replace("index.html");
}

function listings() {
    window.location.replace("listings.html");
}

//this is getting to be a big oh file might have to split it up
